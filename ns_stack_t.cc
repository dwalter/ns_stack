/*
    ns_stack_t.h : example source for non-stop wait-free/weight-less
      [ low fat ] locking using extra bits in 64bit pointer
      
    Copyright (C) 2012 David Walter<walter.david@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

 */

#include <signal.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ns_stack/ns_stack_t.h>

ns::ns_stack_t< int > stack ;

void* start( void* arg )
{
  uint64_t tries( 10 ) ;
  while ( tries-- )
  {
    for ( int i = 0 ; i < 100 ; i++ )
    {
      try
      {
	stack.push( i ) ;
      }
      catch( std::exception& e )
      {
	std::cerr << e.what() << std::endl ;
      }
    }
    while ( int* x = stack.pop() )
    {
      try
      {
	if ( x )
	  std::cerr << *x << ' ' ;
      }
      catch( std::exception& e )
      {
	std::cerr << e.what() << std::endl ;
      }
    }
    std::cerr << std::endl ;
  }
  pthread_exit(0) ;
}

void handler       ( int signal )
{
  std::cerr << "signal[" << sys_siglist[signal] << std::endl ;
}

void* handler_thread( void* )
{
  sigset_t set;
  sigfillset( &set ) ;
  struct sigaction action;
  action.sa_flags = SA_SIGINFO;
  action.sa_handler = handler ;
  sigaction(SIGALRM, &action, NULL);
  sigaddset( &set, SIGUSR1 ) ;
  sigaddset( &set, SIGUSR2 ) ;
  sigaddset( &set, SIGTERM ) ;
  sigaddset( &set, SIGQUIT ) ;
  sigaddset( &set, SIGHUP  ) ;
  sigaddset( &set, SIGFPE  ) ;
  sigaddset( &set, SIGILL  ) ;
  sigaddset( &set, SIGSEGV ) ;
  sigaddset( &set, SIGBUS  ) ;
  sigaddset( &set, SIGABRT ) ;
  sigaddset( &set, SIGTRAP ) ;
  sigaddset( &set, SIGSYS  ) ;
  sigaddset( &set, SIGINT ) ;
  sigaddset( &set, SIGPIPE ) ;

  sigaction( SIGALRM, &action, 0 );
  sigaction( SIGUSR1, &action, 0 ) ;
  sigaction( SIGUSR2, &action, 0 ) ;
  sigaction( SIGTERM, &action, 0 ) ;
  sigaction( SIGQUIT, &action, 0 ) ;
  sigaction( SIGHUP , &action, 0 ) ;
  sigaction( SIGFPE , &action, 0 ) ;
  sigaction( SIGILL , &action, 0 ) ;
  sigaction( SIGSEGV, &action, 0 ) ;
  sigaction( SIGBUS , &action, 0 ) ;
  sigaction( SIGABRT, &action, 0 ) ;
  sigaction( SIGTRAP, &action, 0 ) ;
  sigaction( SIGSYS , &action, 0 ) ;
  sigaction( SIGINT , &action, 0 ) ;
  sigaction( SIGPIPE, &action, 0 ) ;

  pthread_sigmask(SIG_UNBLOCK, &set, NULL);

  while( true )
  {
    timespec ts = {1,0} ;
    nanosleep( & ts, 0 ) ;
  }
  return 0 ;
}

int main( int argc, char** argv )
{
  sigset_t set;
  sigfillset( &set ) ;
  pthread_sigmask( SIG_BLOCK, &set, 0 ) ;
  pthread_t signal_thread ;
  pthread_create( &signal_thread, 0, &handler_thread, 0 ) ;

  pthread_attr_t attr;
  size_t stacksize;

  pthread_attr_init(&attr);
  pthread_attr_getstacksize(&attr, &stacksize); 
  // printf("%u\n", stacksize);

  try
  {
    const int max_threads( getenv( "THREADS" ) ? atoi( getenv( "THREADS" ) ) : 2 ) ;
    pthread_t thread[max_threads] ;
    for ( int i=0; i < max_threads; i++ )
    {
      // stacksize = 4194304 ;
      // stacksize = 16777216 ;
      stacksize = 1 << 16 ;
      pthread_attr_setstacksize(&attr, stacksize); 
      pthread_create( &thread[i], &attr, &start, reinterpret_cast<void*>( & stack ) ) ;
    }
    for ( int i=0; i < max_threads; i++ )
    {
      pthread_join( thread[i], 0 ) ;
    }
    std::ofstream o( "done" ) ;
    o << "done" << std::endl ;
  }
  catch( ... )
  {
  }
  return 0 ;
}
